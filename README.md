# Java'da Library ve Framework, Spring ve Maven


---

## 📚 Kütüphane ve Framework

### Kütüphane (Library):

- **Kullanım Amacı:**
  - Belirli işlevleri yerine getiren ve doğrudan çağrılabilen kod parçalarının bir koleksiyonudur.
  - Tekrar kullanılabilir bileşenler içerir ve programcılara belirli görevleri yerine getirmede yardımcı olur.

- **Çağrılma Yöntemi:**
  - Kullanıcı tarafından ihtiyaç duyulduğunda doğrudan çağrılır.
  - Kodun belirli bir bölümünde kullanılmak üzere projeye dahil edilir ve işlevi o noktada gerçekleştirilir.

- **Yapı ve Organizasyon:**
  - Genellikle tek başına çalışabilir ve bağımsızdır.
  - Kodun parçaları genellikle farklı dosyalara veya modüllere bölünmüştür.

### Framework:

- **Kullanım Amacı:**
  - Geniş kapsamlı bir yapı sunar ve bir uygulamanın temel mimarisini sağlar.
  - Kodun nasıl organize edileceği, akışın nasıl kontrol edileceği gibi temel prensipleri belirler.

- **Çağrılma Yöntemi:**
  - Uygulama tarafından kontrol edilir ve uygulama çalıştırıldığında otomatik olarak devreye girer.
  - Kodun farklı bölümlerinde doğrudan çağrılmaz, genellikle framework tarafından sağlanan yapılar ve araçlar kullanılarak etkileşime geçilir.

- **Yapı ve Organizasyon:**
  - Modüler bir yapıya sahiptir ve birçok bileşeni içerir.
  - Uygulama, framework tarafından sağlanan yapıya uygun olarak organize edilir ve geliştiriciler, framework'ün sunduğu araçları kullanarak uygulama geliştirirler.

### Farklılık Özeti:

- Kütüphaneler, belirli işlevleri yerine getiren tekrar kullanılabilir kod parçalarıdır, ancak framework'ler daha kapsamlı bir yapı sunar ve bir uygulamanın temel mimarisini sağlar.
- Kütüphaneler doğrudan çağrılırken, framework'ler genellikle uygulama tarafından kontrol edilir.
- Kütüphaneler genellikle bağımsız çalışabilirken, framework'ler genellikle uygulama ile sıkı bir şekilde entegre edilir ve uygulamanın geliştirilmesini yönlendirir.

---

## 🌱 Spring Framework

Spring, Java uygulamaları için güçlü bir çalışma çerçevesidir ve geniş bir ekosisteme sahiptir.

### Temel Özellikler:
- **IOC (Inversion of Control):** Nesne oluşturma sürecini tersine çevirir ve bağımlılık enjeksiyonunu sağlar.
- **AOP (Aspect-Oriented Programming):** Başka bir kod parçasının (örneğin, günlüğe yazma veya güvenlik) uygulamanın herhangi bir noktasına eklenmesini sağlar.
- **MVC (Model-View-Controller):** Web uygulamaları için bir mimari modeli sağlar.

### Spring Boot:
Spring Boot, Spring uygulamalarının hızlı ve kolay bir şekilde geliştirilmesini sağlayan bir araçtır.

- **Otomatik Yapılandırma:** Standart yapılandırma gereksinimlerini otomatikleştirir ve varsayılan değerler sağlar.
- **Gömülü Sunucu Desteği:** Gömülü web sunucusu sağlar ve dışarıdan bir sunucu gerektirmez.
- **Bağımlılık Yönetimi:** Maven veya Gradle gibi araçlarla bağımlılık yönetimini kolaylaştırır.

#### Spring Boot Temel Anotasyonları:

 **__\@SpringBootApplication:__**
   - Spring Boot uygulamalarının ana sınıfına bu anotasyon eklenir.
   - Bu anotasyon, `@Configuration`, `@EnableAutoConfiguration` ve `@ComponentScan` anotasyonlarını içerir ve Spring Boot uygulamasını yapılandırır ve başlatır.

 **__\@Configuration:__**
   - Spring IOC Container'a özelleştirilmiş bean'ler (nesneler) eklemek için kullanılır.
   - `@Bean` anotasyonu ile birlikte kullanılarak, özel olarak yapılandırılmış bean'lerin tanımlanmasını sağlar.

 **__@EnableAutoConfiguration:__**
   - Otomatik yapılandırmayı etkinleştirir.
   - Spring Boot uygulamasının başlatılması sırasında, kullanılan bağımlılıklar ve projenin yapılandırması analiz edilerek, gerekli Spring bean'leri otomatik olarak yapılandırır.

 **__@ComponentScan:__**
   - Bileşen taramayı etkinleştirir.
   - Spring Boot uygulamasında bileşenleri (Component, Service, Repository, Controller) tarar ve Spring tarafından yönetilen bileşenleri bulur ve uygulamaya ekler.

 **__\@Bean:__**
   - Spring IOC Container'a özelleştirilmiş bean'lerin (nesnelerin) eklenmesini sağlar.
   - `@Configuration` sınıflarında veya `@Component` sınıflarında kullanılır.

 **__\@Autowired:__**
   - Bağımlılık enjeksiyonunu gerçekleştirmek için kullanılır.
   - Spring tarafından yönetilen bileşenlerin (bean'lerin) diğer bean'lere otomatik olarak enjekte edilmesini sağlar.

 **__\@Service:__**
   - Servis sınıflarını işaretlemek için kullanılır.
   - İş mantığının bulunduğu sınıfların üzerine bu anotasyon eklenir.
   - Servis sınıfları genellikle iş mantığını yürütür ve veri işlemlerini gerçekleştirir.

 **__\@Controller:__**
   - Web isteklerini işleyen kontrol sınıflarını işaretlemek için kullanılır.
   - MVC (Model-View-Controller) mimarisinde Controller katmanını temsil eder.
   - HTTP isteklerini alır, işler ve uygun cevapları döndürür.

 **__\@Repository:__**
   - Veritabanı işlemlerini gerçekleştiren veri erişim sınıflarını işaretlemek için kullanılır.
   - Veri erişim katmanının bir parçasıdır ve genellikle veritabanı işlemlerini yürütür.
   - JPA (Java Persistence API) veya Spring Data gibi teknolojilerle entegre edilerek kullanılır.

---

## 🔄 IOC (Inversion of Control)

IOC (Inversion of Control), Java uygulamalarının geliştirilmesinde önemli bir prensiptir. 

- **Kontrol Akışı Değişimi:** Uygulamanın kontrol akışı dış kaynaklara bırakılır ve dışarıdan yönetilir.
- **Dependency Injection (DI):** Nesne oluşturma ve bağımlılıkların yönetimi DI ile gerçekleştirilir.
- **Daha Test Edilebilir Kod:** Bağımlılıkların enjekte edilmesi, kodun daha az bağımlı ve daha test edilebilir olmasını sağlar.

---

## IOC Container (Inversion of Control Container) Nedir?

IOC Container, bir Java uygulamasında bağımlılıkların yönetildiği ve IOC prensibini uygulayan bir bileşendir. Spring Framework, IOC Container özelliğini kullanarak nesne yönetimini gerçekleştirir.

### Spring Boot'ta IOC Container Nasıl Çalışır?

1. **Tanımlama:**
   Spring Boot uygulamasında, IOC Container'ı kullanmak için bean (fasıla) adını verdiğimiz bileşenleri tanımlarız. Bu bean'ler, genellikle `@Component`, `@Service`, `@Repository` gibi Spring tarafından sağlanan belirli anotasyonlarla işaretlenirler.

2. **Tarama (Scanning):**
   Spring Boot, bileşenleri otomatik olarak taramak için bileşen tarama (component scanning) özelliğini kullanır. Proje başlatıldığında, Spring, bileşenlerin paketlerini tarar ve işaretlenmiş olanları IOC Container'a ekler.

3. **Bağımlılık Enjeksiyonu (Dependency Injection):**
   IOC Container, bağımlılıkların enjekte edilmesini yönetir. Spring Boot, `@Autowired` anotasyonu ile bağımlılıkların (diğer bean'lerin) otomatik olarak enjekte edilmesini sağlar. Böylece, bir bileşen başka bir bileşeni referans alabilir ve kullanabilir.

4. **Hayat Döngüsü Yönetimi:**
   IOC Container, Spring Bean'lerinin hayat döngüsünü yönetir. Bean'lerin oluşturulması, yapılandırılması, kullanıma hazır hale getirilmesi ve yok edilmesi gibi işlemler IOC Container tarafından otomatik olarak gerçekleştirilir.

5. **Özel Konfigürasyonlar:**
   Spring Boot, IOC Container'ı özelleştirmek ve özel konfigürasyonları tanımlamak için `@Configuration` ve `@Bean` anotasyonlarını kullanır. Bu şekilde, özel bileşenlerin veya servislerin tanımlanması ve IOC Container'a eklenmesi sağlanır.





## 🛠️ Maven

Maven, Java projelerinin yönetimi, bağımlılıkların yönetimi ve proje otomasyonu için kullanılan bir araçtır. Maven, proje bağımlılıklarını yerel depo (local repository) ve merkezi depo (central repository) gibi depolardan alır ve yönetir.

### Maven'in Depo Akışı:

1. **Yerel Depo (Local Repository):** Maven, proje bağımlılıklarını yerel bir depoda saklar. Varsayılan olarak, yerel depo, kullanıcının ana dizinindeki `.m2/repository` dizininde bulunur. Maven, bağımlılıkları yerel depodan alır ve projeye dahil eder. Ayrıca, projenin üretilmiş (compiled) dosyalarını da yerel depoda saklar.

2. **Merkezi Depo (Central Repository):** Maven, proje bağımlılıklarını varsayılan olarak Maven Merkez Deposu'ndan (Maven Central Repository) alır. Maven Central Repository, açık kaynaklı Java projeleri için geniş bir bağımlılık koleksiyonuna sahip genel bir depodur. Maven, projenin `pom.xml` dosyasındaki bağımlılıkları merkezi depodan çeker ve yerel depoya indirir. Bu sayede projenin bağımlılıkları otomatik olarak çözülür ve projenin yapısı oluşturulur.



Maven normalde ilk önce yerel depoya (local repository) bakar ve bağımlılıkları orada bulursa, yeniden indirme ihtiyacı duymaz. Ancak, yerel depoda bulunmayan veya güncel olmayan bağımlılıklar için Maven, merkezi Maven Deposu (Central Repository) veya diğer belirtilmiş uzak depoları arar ve gerekli bağımlılıkları bu merkezi depolardan indirir.

Maven projesi geliştirilirken, projenin bağımlılıkları yerel depoya indirilir ve yerel depoda saklanır. Bu, projenin çalışmasını hızlandırır çünkü her derlemede veya projenin farklı bir modülünde aynı bağımlılıkların tekrar tekrar indirilmesine gerek kalmaz. Bu aynı zamanda, projenin bağımlılıklarının internete bağlı olmadan çalışmasını sağlar.

Ancak, yerel depoda bir bağımlılık bulunamazsa veya güncel olmayabilirse, Maven, belirtilen bağımlılığın versiyonunu ve diğer gerekli bilgileri merkezi Maven Deposu'ndan almak için internete bağlanır. Merkezi Maven Deposu, geniş bir Maven projesi ve kütüphaneler ekosistemine sahip olduğu için, genellikle ihtiyaç duyulan bağımlılıkların çoğunu içerir.

Merkezi Maven Deposu'ndan bağımlılıkları indirmek, projenin internete bağlı olduğu ve gerektiğinde güncel bağımlılıkları alabileceği anlamına gelir. Bu, projenin güncel ve dışa bağımlılıklarının yönetimini kolaylaştırır ve projenin gelişimi için önemlidir.





# POM XML Dosyası Örneği ve Açıklamaları

Aşağıda, bir Maven projesi için örnek bir `pom.xml` dosyası ve her bir bölümün açıklamaları yer alır:

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <!-- Kök element, dosyanın bir Maven POM dosyası olduğunu ve gerekli XML schema tanımlamalarını belirtir -->

    <modelVersion>4.0.0</modelVersion>
    <!-- POM model versiyonunu belirtir, bu örnek için 4.0.0 kullanılmaktadır -->

    <groupId>com.example</groupId>
    <!-- Proje grubunun global benzersiz identifier'ı, genellikle şirket domain isminin ters çevrilmiş hali -->

    <artifactId>my-project</artifactId>
    <!-- Maven repository'de projenin ismi olarak kullanılacak artifact ID -->

    <version>1.0-SNAPSHOT</version>
    <!-- Projenin şu anki versiyon bilgisini belirtir. 'SNAPSHOT' sürümü geliştirme aşamasında olduğunu gösterir -->

    <packaging>jar</packaging>
    <!-- Proje paketleme tipini belirtir; örneğin jar, war -->

    <dependencies>
        <!-- Projeye dahil edilmesi gereken bağımlılıklar burada listelenir -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>5.3.10</version>
            <!-- Spring Framework'un çekirdek modülüne bağımlılık örneği -->
        </dependency>
    </dependencies>

    <build>
        <!-- Proje inşa ayarlarını içerir -->
        <plugins>
            <!-- Proje inşası için kullanılacak Maven eklentileri -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <!-- Java kaynak kodlarını derlemek için kullanılan Maven Compiler Plugin -->
                <configuration>
                    <source>1.8</source>
                    <!-- Derleme için kaynak Java versiyonu -->
                    <target>1.8</target>
                    <!-- Çıktı olarak üretilen Java byte code versiyonu -->
                </configuration>
            </plugin>
        </plugins>
    </build>

    <properties>
        <!-- Projenin çeşitli yapılandırma değerlerini içerir -->
        <java.version>1.8</java.version>
        <!-- Projenin kullanacağı Java sürümü -->
    </properties>
</project>




 