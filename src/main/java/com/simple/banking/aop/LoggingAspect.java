package com.simple.banking.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;  // AspectJ kütüphanesinden Aspect anotasyonunu içe aktarıyoruz.
import org.aspectj.lang.annotation.Before;  // AspectJ kütüphanesinden Before anotasyonunu içe aktarıyoruz.
import org.aspectj.lang.annotation.After;   // AspectJ kütüphanesinden After anotasyonunu içe aktarıyoruz.
import org.aspectj.lang.annotation.Around;  // AspectJ kütüphanesinden Around anotasyonunu içe aktarıyoruz.
import org.aspectj.lang.ProceedingJoinPoint; // Metodların etrafında loglama yapabilmek için ProceedingJoinPoint sınıfını içe aktarıyoruz.
import org.springframework.stereotype.Component; // Spring'in Component anotasyonunu içe aktarıyoruz.

@Aspect  // Bu sınıfın bir Aspect olduğunu belirtir.
@Slf4j
@Component  // Spring tarafından bir bileşen olarak yönetileceğini belirtir.
public class LoggingAspect {

//    @Before("execution(* com.simple.banking.service.*.*.*(..))")
//    public void testAOP(JoinPoint joinPoint){
//        log.info(joinPoint.getSignature().toString());
//        log.info("service metodu çağrıldı");
//
//    }





    // Bu metod, belirttiğimiz yöntemlerin (execution(* com.yourpackage..*(..))) başlamasından önce çalışır.
    // AspectJ Pointcut ifadesi:
    // - `execution(* com.yourpackage..*(..))`
    //   - `*`: Herhangi bir dönüş tipi olabilir.
    //   - `com.yourpackage..*`: `com.yourpackage` ile başlayan herhangi bir paket içindeki tüm sınıflar ve bu sınıfların tüm metodları.
//    //   - `*(..))`: Herhangi bir isimdeki metod ve bu metodun herhangi bir sayıda parametre alabileceği anlamına gelir.
//    @Before("execution(* com.simple.banking.*.*.*.*(..))")
//    public void logBeforeMethod() {
//        // Metod başlamadan önce bilgi logu yazıyoruz.
//        System.out.println("Cem Cem **********************Cem ");
//
//    }
////
////    // Bu metod, belirttiğimiz yöntemlerin (execution(* com.yourpackage..*(..))) tamamlanmasından sonra çalışır.
//    @After("execution(* com.simple.banking.*.*.*.*(..))")
//    public void logAfterMethod() {
//        // Metod tamamlandıktan sonra bilgi logu yazıyoruz.
//        log.info("Method execution finished");
//    }

//    // Bu metod, belirttiğimiz yöntemlerin (execution(* com.yourpackage..*(..))) etrafında çalışır, yani hem başlamadan önce hem de tamamlandıktan sonra çalışır.
    @Around("execution(* com.simple.banking.*.*.*.*(..))")
    public Object logAroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        // Metod başlamadan önce bilgi logu yazıyoruz.
        log.info("Method execution started: " + joinPoint.getSignature());

        // Metodun devam etmesini sağlıyoruz.
        Object result = joinPoint.proceed();

        // Metod tamamlandıktan sonra bilgi logu yazıyoruz.
        log.info("Method execution finished: " + joinPoint.getSignature());

        // Metodun sonucunu geri döndürüyoruz.
        return result;
    }
}
