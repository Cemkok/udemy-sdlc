package com.simple.banking.controller.mvc;

import com.simple.banking.dto.request.AuthenticationRequest;
import com.simple.banking.dto.request.RegisterRequest;
import com.simple.banking.model.Transaction;
import com.simple.banking.service.concretes.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class AuthenticationMvcController {
    @Autowired
    AuthenticationService authenticationService;

    @GetMapping("/register")
    public String showRegisterForm(Model model) {
        model.addAttribute("registerRequest", new RegisterRequest());
        return "register"; // Thymeleaf şablonunu gösterir (register.html)
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute RegisterRequest registerRequest) {
        // Burada gelen verileri işleyebilirsiniz
        // Örneğin: userService.save(registerRequest);
        authenticationService.register(registerRequest);
        // Kayıt işlemi başarılı olduğunda giriş sayfasına yönlendir
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String showLoginForm(Model model) {
        model.addAttribute("loginRequest", new AuthenticationRequest());
        return "login"; // Thymeleaf şablonunu gösterir (login.html)
    }

    @PostMapping("/login")
    public String loginUser(@ModelAttribute AuthenticationRequest loginRequest, Model model) {
        // Burada authenticationService.login(loginRequest) gibi bir metodla login işlemi yapabilirsiniz.

        if (authenticationService.authenticate(loginRequest).getToken()!=null) {
            return "redirect:/home"; // Başarılı girişten sonra yönlendirme
        } else {
            model.addAttribute("error", "Invalid email or password");
            return "home";
        }


    }
//    @GetMapping("/home")
//    public String showHomePage(Model model) {
//        // Model'e gerekli verileri ekleyin (örneğin, kullanıcı bilgileri, işlem geçmişi, döviz ve altın fiyatları vb.)
//        model.addAttribute("transactionHistory", getTransactionHistory());
//        model.addAttribute("currencyRates", getCurrencyRates());
//        return "home";
//    }

    // Örnek olarak kullanıcı işlem geçmişini döndüren bir metot
//    private List<Transaction> getTransactionHistory() {
//        // Burada kullanıcıya ait işlem geçmişini veritabanından çekin
//        return Arrays.asList(
//                new Transaction("Havale", "TR1234567890", "TR0987654321", 500.0, "2023-06-27"),
//                new Transaction("Fatura Ödeme", "Elektrik", null, 150.0, "2023-06-25"),
//                new Transaction("Kredi Çekme", "Konut Kredisi", null, 100000.0, "2023-06-20")
//        );
    }

    // Örnek olarak döviz ve altın fiyatlarını döndüren bir metot


