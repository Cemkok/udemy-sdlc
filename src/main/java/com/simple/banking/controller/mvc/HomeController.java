package com.simple.banking.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(Model model) {
        // Örnek kullanıcı ve hesap bilgileri
        model.addAttribute("userName", "Ahmet");
        model.addAttribute("accountBalance", "12,345.67 TL");

        // Örnek döviz kurları
        Map<String, String> currencyRates = new HashMap<>();
        currencyRates.put("USD", "1.09");
        currencyRates.put("EUR", "1.15");
        currencyRates.put("GBP", "1.25");
        currencyRates.put("Gold", "58.32");
        model.addAttribute("currencyRates", currencyRates);

        return "home";
    }
}
