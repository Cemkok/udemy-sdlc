package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.BankAccountRequestDTO;
import com.simple.banking.dto.response.BankAccountResponseDTO;
import com.simple.banking.model.BankAccount;
import com.simple.banking.repository.BankAccountRepository;
import com.simple.banking.security.User;
import com.simple.banking.service.abstracts.BankAccountService;
import com.simple.banking.util.TransactionUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/bankAccount")
public class BankAccountController {
    @Autowired
    BankAccountService bankAccountService;

    @PostMapping
    public BankAccountResponseDTO addBankAccount(@TransactionUser User user, @RequestBody BankAccountRequestDTO bankAccountRequestDTO){
        System.out.println(user);
        return bankAccountService.addBankAccount(bankAccountRequestDTO);
    }
    @GetMapping
    public List<BankAccount> getAllBankAccount(){
        return bankAccountService.getAllBankAccount();
    }
}
