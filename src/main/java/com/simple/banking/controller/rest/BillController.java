package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.BillPaymentRequestDTO;
import com.simple.banking.dto.request.BillResponseRecord;
import com.simple.banking.service.abstracts.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/v1/bill/")
public class BillController {

    @Autowired
    private BillService billService;

    @PostMapping("payment/{billId}")
    public ResponseEntity<String> payBill(@PathVariable String billNumber) {
        boolean success = billService.payBill(billNumber);
        if (success) {
            return new ResponseEntity<>("Bill payment successful", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Bill payment failed", HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/get-bill/{idNumber}")
    public Flux<BillResponseRecord> getBills() {
        return billService.getBills();
    }
}
