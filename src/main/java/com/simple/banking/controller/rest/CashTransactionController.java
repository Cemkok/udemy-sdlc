package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.CashTransactionRequestDTO;
import com.simple.banking.dto.response.CashTransactionResponseDTO;
import com.simple.banking.model.CashTransaction;
import com.simple.banking.service.abstracts.CashTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/cashTransaction")
public class CashTransactionController {

    @Autowired
    private CashTransactionService cashTransactionService;

    @GetMapping
    public List<CashTransactionResponseDTO> getAllCashTransactions() {
        return cashTransactionService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CashTransaction> getCashTransactionById(@PathVariable Long id) {
        Optional<CashTransaction> cashTransaction = cashTransactionService.findById(id);
        return cashTransaction.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/deposit")
    public ResponseEntity<CashTransactionResponseDTO> deposit(@RequestBody CashTransactionRequestDTO cashTransactionRequestDTO) {
        CashTransactionResponseDTO savedTransaction = cashTransactionService.deposit(cashTransactionRequestDTO);
        return ResponseEntity.ok(savedTransaction);
    }

    @PostMapping("/withdraw")
    public ResponseEntity<CashTransactionResponseDTO> withdraw(@RequestBody CashTransactionRequestDTO cashTransactionRequestDTO) {
        try {
            CashTransactionResponseDTO savedTransaction = cashTransactionService.withdraw(cashTransactionRequestDTO);
            return ResponseEntity.ok(savedTransaction);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

//    @PutMapping("/{id}")
//    public ResponseEntity<CashTransaction> updateCashTransaction(@PathVariable Long id, @RequestBody CashTransaction cashTransactionDetails) {
//        Optional<CashTransaction> optionalCashTransaction = cashTransactionService.findById(id);
//        if (optionalCashTransaction.isPresent()) {
//            CashTransaction cashTransaction = optionalCashTransaction.get();
//            cashTransaction.setAmount(cashTransactionDetails.getAmount());
//            cashTransaction.setType(cashTransactionDetails.getType());
//            cashTransaction.setBankAccount(cashTransactionDetails.getBankAccount());
//            final CashTransaction updatedCashTransaction = cashTransactionService.save(cashTransaction);
//            return ResponseEntity.ok(updatedCashTransaction);
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCashTransaction(@PathVariable Long id) {
        Optional<CashTransaction> optionalCashTransaction = cashTransactionService.findById(id);
        if (optionalCashTransaction.isPresent()) {
            cashTransactionService.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }


}
