package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.CreditApplicationRequest;
import com.simple.banking.dto.response.CreditApplicationResponse;
import com.simple.banking.service.concretes.CreditApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/credit-applications")
public class CreditApplicationController {

    @Autowired
    private CreditApplicationService creditApplicationService;

    @PostMapping
    public ResponseEntity<CreditApplicationResponse> applyForCredit(@RequestBody CreditApplicationRequest request) {
        CreditApplicationResponse response = creditApplicationService.applyForCredit(request);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CreditApplicationResponse> getCreditApplicationById(@PathVariable Long id) {
        CreditApplicationResponse response = creditApplicationService.getCreditApplicationById(id);
        return ResponseEntity.ok(response);
    }

    // Diğer kredi başvuru işlemleri
}
