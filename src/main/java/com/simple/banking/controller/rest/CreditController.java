package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.CreditRequestDTO;
import com.simple.banking.model.Credit;
import com.simple.banking.service.abstracts.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/v1/credit")
public class CreditController {

    @Autowired
    CreditService creditService;

    @PostMapping
    public Credit addCredit(@RequestBody CreditRequestDTO creditRequestDTO){
        return creditService.addCredit(creditRequestDTO);
    }

    @GetMapping
    public List<Credit> getAllCredit(){
        return creditService.getAllCredit();
    }

    @GetMapping("/{id}")
    public Credit getCreditById(@PathVariable("id") Long id) {
        return creditService.findCreditById(id);
    }

    @PutMapping("/{id}")
    public Credit updateCredit(@PathVariable("id") Long id, @RequestBody CreditRequestDTO creditRequestDTO) {
        Credit existingCredit = creditService.findCreditById(id);

        if (existingCredit == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credit not found with id: " + id);
        }

        existingCredit.setCreditType(creditRequestDTO.getCreditType());
        existingCredit.setRate(creditRequestDTO.getRate());
        existingCredit.setMinimumCreditScore(creditRequestDTO.getMinimumCreditScore());
        existingCredit.setMinimumAmount(creditRequestDTO.getMinimumAmount());
        existingCredit.setMaximumAmount(creditRequestDTO.getMaximumAmount());
        existingCredit.setMinimumTerm(creditRequestDTO.getMinimumTerm());
        existingCredit.setMaximumTerm(creditRequestDTO.getMaximumTerm());
        existingCredit.setProcessingFeeRate(creditRequestDTO.getProcessingFeeRate());
        existingCredit.setEarlyRepaymentFeeRate(creditRequestDTO.getEarlyRepaymentFeeRate());

        return creditService.updateCredit(existingCredit);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCredit(@PathVariable("id") Long id) {
        Credit existingCredit = creditService.findCreditById(id);

        if (existingCredit == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credit not found with id: " + id);
        }

        creditService.deleteCredit(id);
        return ResponseEntity.ok().build();
    }
}
