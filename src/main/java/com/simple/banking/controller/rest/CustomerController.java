package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.CustomerRequestDTO;
import com.simple.banking.dto.response.CustomerResponseDTO;
import com.simple.banking.model.Customer;
import com.simple.banking.repository.CustomerRepository;
import com.simple.banking.service.abstracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;
    @Autowired
    CustomerRepository customerRepository;
    @GetMapping
    public List<CustomerResponseDTO> getAllCustomer(){
        return customerService.getAll();
    }
    @PostMapping
    public CustomerResponseDTO addCustomer(@RequestBody CustomerRequestDTO customerRequestDTO){
        return customerService.addCustomer(customerRequestDTO);
    }
    @PutMapping("{identityNumber}")
    public ResponseEntity<?> updateCustomer(@RequestBody Customer customerToUpdate, @PathVariable String identityNumber){
        Customer customer = customerRepository.findByIdentityNumber(identityNumber);
        customer.setSurname(customerToUpdate.getSurname());
        customerRepository.save(customer);
        return  ResponseEntity.ok("resource updated");
    }
}
