package com.simple.banking.controller.rest;

import com.simple.banking.dto.request.EmailRequest;
import com.simple.banking.email.EmailService;
import com.simple.banking.email.PdfService;
import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
@RestController
@RequestMapping("/email")
public class EmailController {
    @Autowired
    private PdfService pdfService;

    @Autowired
    private EmailService emailService;

    @PostMapping("/sendEmail")
    public String sendEmail(@RequestBody EmailRequest emailRequest) {
        emailService.sendSimpleMessage(emailRequest.getTo(), emailRequest.getSubject(), emailRequest.getText());
        return "Email sent successfully";
    }
    @GetMapping("/sendPdfEmail")
    public String sendPdfEmail(@RequestParam String to, @RequestParam String subject, @RequestParam String text) {
        try {
            // PDF dosyasını oluştur
            String pdfPath = pdfService.createPdf("Bu bir test PDF dosyasıdır.");

            // PDF dosyasını e-posta ile gönder
            emailService.sendMessageWithAttachment(to, subject, text, pdfPath);

            return "E-posta başarıyla gönderildi!";
        } catch (IOException | MessagingException e) {
            e.printStackTrace();
            return "E-posta gönderilirken bir hata oluştu: " + e.getMessage();
        }
    }


}
