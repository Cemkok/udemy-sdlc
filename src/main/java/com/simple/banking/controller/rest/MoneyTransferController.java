package com.simple.banking.controller.rest;

import com.simple.banking.service.abstracts.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/transfer")
public class MoneyTransferController {

    @Autowired
    private MoneyTransferService moneyTransferService;

    @PostMapping("/havale")
    public ResponseEntity<String> makeHavale(@RequestParam String sourceAccountNumber,
                                             @RequestParam String targetAccountNumber,
                                             @RequestParam Double amount) {
        try {
            moneyTransferService.makeHavale(sourceAccountNumber, targetAccountNumber, amount);
            return ResponseEntity.ok("Havale işlemi başarılı");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Hata: " + e.getMessage());
        }
    }

    @PostMapping("/eft")
    public ResponseEntity<String> makeEft(@RequestParam String sourceAccountNumber,
                                          @RequestParam String targetAccountNumber,
                                          @RequestParam Double amount) {
        try {
            moneyTransferService.makeEFT(sourceAccountNumber, targetAccountNumber, amount);
            return ResponseEntity.ok("EFT işlemi başarılı");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Hata: " + e.getMessage());
        }
    }
}
