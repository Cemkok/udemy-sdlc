package com.simple.banking.dto.request;

import lombok.*;


public record ApprovalRequest(String accountNumber,Double amount) {

}
