package com.simple.banking.dto.request;

import jakarta.websocket.server.ServerEndpoint;
import lombok.Getter;
import lombok.Setter;

@Getter
public class BankAccountRequestDTO {
    private Double balance;
    private String identityNumber;

}
