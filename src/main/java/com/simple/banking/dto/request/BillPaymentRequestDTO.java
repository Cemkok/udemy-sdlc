package com.simple.banking.dto.request;

import lombok.Data;

@Data
public class BillPaymentRequestDTO {
    private Long billId;
}
