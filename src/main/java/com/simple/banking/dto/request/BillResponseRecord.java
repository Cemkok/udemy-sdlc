package com.simple.banking.dto.request;

public record BillResponseRecord( String billType,
                                  Long billId,
                                  String billNumber,
                                  Double billAmount) {
}
