package com.simple.banking.dto.request;

import lombok.Getter;

@Getter
public class CashTransactionRequestDTO {
    private String bankAccountNumber;
    private Double amount;
}
