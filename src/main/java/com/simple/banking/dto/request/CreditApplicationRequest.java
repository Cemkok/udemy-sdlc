package com.simple.banking.dto.request;

import lombok.Getter;

@Getter
public class CreditApplicationRequest {
    private Long creditId;
    private String accountNumber;
    private Double amount;
    private int term;

}