package com.simple.banking.dto.request;

import com.simple.banking.enums.CreditType;
import jakarta.persistence.Entity;
import lombok.Data;
import lombok.Getter;

@Getter
public class CreditRequestDTO {

        private CreditType creditType;
        private double rate;
        private int minimumCreditScore;
        private double minimumAmount;
        private double maximumAmount;
        private int minimumTerm;
        private int maximumTerm;
        private double processingFeeRate;
        private double earlyRepaymentFeeRate;

}
