package com.simple.banking.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreditScoreRequest {
    private String identityNumber;
}
