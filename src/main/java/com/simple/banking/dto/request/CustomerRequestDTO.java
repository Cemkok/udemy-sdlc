package com.simple.banking.dto.request;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class CustomerRequestDTO {
    private String name;
    private String surname;
    private String identityNumber;
    private LocalDate dateOfBirth;
}
