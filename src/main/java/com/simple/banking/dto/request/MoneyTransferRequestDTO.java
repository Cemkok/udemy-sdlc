package com.simple.banking.dto.request;

import lombok.Getter;

@Getter
public class MoneyTransferRequestDTO {
    private Double amount;
    private String sourceAccountNumber;
    private String targetAccountNumber;
}
