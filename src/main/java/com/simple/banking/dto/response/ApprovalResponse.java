package com.simple.banking.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApprovalResponse {
    private boolean approved;
    private String message;

}
