package com.simple.banking.dto.response;

import com.simple.banking.model.CashTransaction;
import com.simple.banking.model.Customer;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
public class BankAccountResponseDTO {
    private Long accountId;
    private String accountNumber;
    private Double balance;
    private Set<CashTransactionResponseDTO> cashTransactions = new HashSet<>();
    private CustomerResponseDTO customer;

}
