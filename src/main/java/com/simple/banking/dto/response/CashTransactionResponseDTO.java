package com.simple.banking.dto.response;

import com.simple.banking.enums.CashTransactionType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CashTransactionResponseDTO {
    private Long id;
    private Double amount;
    private CashTransactionType type;
    private String accountNumber;
}
