package com.simple.banking.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreditApplicationResponse {
    private Long applicationId;
    private String status;
    private String message;
}
