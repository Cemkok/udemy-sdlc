package com.simple.banking.dto.response;

import lombok.Getter;

@Getter
public class CreditScoreResponse {
    private int creditScore;
}
