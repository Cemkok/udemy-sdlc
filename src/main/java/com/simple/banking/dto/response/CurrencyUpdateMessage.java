package com.simple.banking.dto.response;

import lombok.Data;

@Data
public class CurrencyUpdateMessage {
    private double usdRate;
    private double eurRate;
    private double gbpRate;
}
