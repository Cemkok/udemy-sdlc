package com.simple.banking.dto.response;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerResponseDTO {
    private String name;
    private String surname;
    private String identityNumber;
}
