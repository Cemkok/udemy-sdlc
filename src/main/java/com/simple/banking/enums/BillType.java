package com.simple.banking.enums;

public enum BillType {
    ELECTRICITY,
    WATER,
    GAS,
    INTERNET,
    PHONE
}
