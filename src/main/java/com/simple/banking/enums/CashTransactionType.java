package com.simple.banking.enums;

public enum CashTransactionType {
    WITHDRAW,
    DEPOSIT,
    MONEY_TRANSFER,

}
