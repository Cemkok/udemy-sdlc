package com.simple.banking.enums;

public enum CreditType {
        PERSONAL_LOAN,
        VEHICLE_LOAN,
        MORTGAGE_LOAN,
        EDUCATION_LOAN,
        SPECIAL_OCCASION_LOAN
}
