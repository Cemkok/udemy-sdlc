package com.simple.banking.enums;

public enum PaymentStatus {
    PAID,
    UNPAID
}
