package com.simple.banking.enums;

public enum Role {
    USER,
    ADMIN
}
