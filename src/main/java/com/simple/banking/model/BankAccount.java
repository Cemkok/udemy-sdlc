package com.simple.banking.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Set;

@Entity
@Table(name = "bank_account")
@Data
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bank_account_id")
    private Long accountId;

    @Column(name = "account_number", unique = true)
    private String accountNumber;

    private Double balance;

    @OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<CashTransaction> cashTransactions;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_identity_number", referencedColumnName = "identity_number")
    private Customer customer;

    @OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<CreditApplication> creditApplications;




}