package com.simple.banking.model;

import com.simple.banking.enums.CashTransactionType;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "cash_transaction")
@Data
public class CashTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Long id;

    private Double amount;
    @Enumerated(EnumType.STRING)
    private CashTransactionType type;
    @ManyToOne(fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_account_number",  referencedColumnName = "account_number")
    private BankAccount bankAccount;

}