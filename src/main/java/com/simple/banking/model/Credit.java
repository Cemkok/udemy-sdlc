package com.simple.banking.model;


import com.simple.banking.enums.CreditType;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Set;

@Entity(name = "credit")
@Data
public class Credit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "credit_type", nullable = false)
    private CreditType creditType;

    @Column(name = "rate", nullable = false)
    private double rate;

    @Column(name = "minimum_credit_score", nullable = false)
    private int minimumCreditScore;

    @Column(name = "minimum_amount", nullable = false)
    private double minimumAmount;

    @Column(name = "maximum_amount", nullable = false)
    private double maximumAmount;

    @Column(name = "minimum_term", nullable = false)
    private int minimumTerm;

    @Column(name = "maximum_term", nullable = false)
    private int maximumTerm;

    @Column(name = "processing_fee_rate", nullable = false)
    private double processingFeeRate;

    @Column(name = "early_repayment_fee_rate", nullable = false)
    private double earlyRepaymentFeeRate;

    @OneToMany(mappedBy = "credit")
    private Set<CreditApplication> creditApplications;
}