package com.simple.banking.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;


@Entity
@Table(name = "credit_application")
@Data
public class CreditApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "amount", nullable = false)
    private Double amount;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "application_date")
    private Date applicationDate;

    @Column(name = "processing_fee")
    private double processingFee;

    @Column(name = "loan_disbursement_amount")
    private double loanDisbursementAmount;

    @Column(name = "total_interest")
    private double totalInterest;

    @Column(name = "total_repayment")
    private double totalRepayment;



    @ManyToOne
    @JoinColumn(name = "credit_id", nullable = false)
    private Credit credit;

    @ManyToOne
    @JoinColumn(name = "bank_account_id", nullable = false)
    private BankAccount bankAccount;
}
