package com.simple.banking.model;
import jakarta.persistence.*;

@Entity
@Table(name = "transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private int transactionId;
    private double amount;
    private String type;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private BankAccount bankAccount;

}