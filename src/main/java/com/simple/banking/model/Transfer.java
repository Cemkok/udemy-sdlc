//package com.simple.banking.model;
//
//import jakarta.persistence.*;
//
//@Entity
//@Table(name = "transfer")
//public class Transfer {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private String transferId;
//
//    private double amount;
//
//    private String senderAccount;
//
//    private String receiverAccount;
//
//    @ManyToOne
//    @JoinColumn(name = "sender_id")
//    private Customer sender;
//
//    @ManyToOne
//    @JoinColumn(name = "receiver_id")
//    private Customer receiver;
//
//}