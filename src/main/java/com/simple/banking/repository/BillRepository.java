package com.simple.banking.repository;

import com.simple.banking.enums.BillType;
import com.simple.banking.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {
    List<Bill> findByBillType(BillType type);
}
