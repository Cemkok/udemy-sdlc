package com.simple.banking.repository;

import com.simple.banking.model.EFT;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EFTRepository extends JpaRepository<EFT, Long> {

}
