package com.simple.banking.repository;

import com.simple.banking.model.Havale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HavaleRepository extends JpaRepository<Havale,Long> {
}
