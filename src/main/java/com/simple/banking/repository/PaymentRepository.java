package com.simple.banking.repository;

import com.simple.banking.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<BankAccount, Long> {
}
