package com.simple.banking.security;

import java.security.SecureRandom;
import java.util.Base64;

public class SecretKeyGenerator {

    public static void main(String[] args) {
        // Secret key uzunluğu (ör. 256 bit = 32 byte)
        int keyLength = 32;

        // Rastgele bir secret key oluştur
        byte[] secretKey = generateRandomKey(keyLength);

        // Base64 ile encode et
        String base64Key = Base64.getEncoder().encodeToString(secretKey);

        // Base64 formatında yazdır
        System.out.println("Base64 Secret Key: " + base64Key);

        // Hexadecimal ile encode et
        String hexKey = bytesToHex(secretKey);

        // Hexadecimal formatında yazdır
        System.out.println("Hex Secret Key: " + hexKey);
    }

    // Rastgele bir byte array oluşturan metot
    private static byte[] generateRandomKey(int length) {
        byte[] key = new byte[length];
        new SecureRandom().nextBytes(key);
        return key;
    }

    // Byte array'i hexadecimal string'e çeviren metot
    private static String bytesToHex(byte[] bytes) {
        StringBuilder hexString = new StringBuilder(2 * bytes.length);
        for (byte b : bytes) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
