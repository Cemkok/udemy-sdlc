package com.simple.banking.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(csrf -> csrf.disable()) // CSRF korumasını devre dışı bırakma
                .authorizeHttpRequests(authorize -> authorize

                        .requestMatchers("/**","api/v1/auth/register/**","register","login").permitAll()
                        .anyRequest().authenticated())
                        .sessionManagement(session -> session
                                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // Oturum yönetimi politikasını ayarla
                        )
                        .authenticationProvider(authenticationProvider) // Kendi kimlik doğrulama sağlayıcınızı ekleyin
                        .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class); // Özel filtrenizi ekleyin


        return http.build();
    }
}
