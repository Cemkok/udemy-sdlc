package com.simple.banking.service.abstracts;

import com.simple.banking.dto.request.BankAccountRequestDTO;
import com.simple.banking.dto.response.BankAccountResponseDTO;
import com.simple.banking.model.BankAccount;

import java.util.List;

public interface BankAccountService {
    BankAccountResponseDTO addBankAccount(BankAccountRequestDTO bankAccountRequestDTO);
    List<BankAccount> getAllBankAccount();
}
