package com.simple.banking.service.abstracts;

import com.simple.banking.dto.request.BillResponseRecord;
import com.simple.banking.enums.BillType;
import com.simple.banking.model.Bill;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface BillService {

    List<Bill> getAllBills();

    List<Bill> getBillsByType(BillType type);

    boolean payBill(String billNumber);

    Flux<BillResponseRecord> getBills();
}