package com.simple.banking.service.abstracts;
import com.simple.banking.dto.request.CashTransactionRequestDTO;
import com.simple.banking.dto.response.CashTransactionResponseDTO;
import com.simple.banking.model.CashTransaction;

import java.util.List;
import java.util.Optional;

public interface CashTransactionService {

    List<CashTransactionResponseDTO> findAll();
    Optional<CashTransaction> findById(Long id);
    CashTransactionResponseDTO deposit(CashTransactionRequestDTO cashTransaction);
    CashTransactionResponseDTO withdraw(CashTransactionRequestDTO cashTransaction);

    void deleteById(Long id);
}
