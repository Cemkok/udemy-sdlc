package com.simple.banking.service.abstracts;

import com.simple.banking.dto.request.CreditRequestDTO;
import com.simple.banking.model.Credit;
import java.util.List;

public interface CreditService {
    Credit addCredit(CreditRequestDTO creditRequestDTO);
    List<Credit> getAllCredit();
    Credit findCreditById(Long id);
    Credit updateCredit(Credit credit);
    void deleteCredit(Long id);
}
