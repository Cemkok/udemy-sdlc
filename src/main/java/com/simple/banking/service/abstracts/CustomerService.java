package com.simple.banking.service.abstracts;


import com.simple.banking.dto.request.CustomerRequestDTO;
import com.simple.banking.dto.response.CustomerResponseDTO;
import com.simple.banking.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CustomerService {
    CustomerResponseDTO addCustomer(CustomerRequestDTO customer);

    List<CustomerResponseDTO> getAll();

    Customer updateCustomer(Customer customer);
}
