package com.simple.banking.service.abstracts;

public interface MoneyTransferService {
    boolean makeHavale(String sourceAccountNumber, String targetAccountNumber, Double amount);
    boolean makeEFT(String sourceAccountNumber, String targetAccountNumber, Double amount);
}
