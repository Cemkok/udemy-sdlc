package com.simple.banking.service.abstracts;

public interface PaymentService {
    boolean payBill(String accountNumber, String billNumber);
}
