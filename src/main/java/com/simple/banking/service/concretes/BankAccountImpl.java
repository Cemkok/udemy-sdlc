package com.simple.banking.service.concretes;

import com.simple.banking.dto.request.BankAccountRequestDTO;
import com.simple.banking.dto.response.BankAccountResponseDTO;
import com.simple.banking.model.BankAccount;
import com.simple.banking.model.Customer;
import com.simple.banking.repository.BankAccountRepository;
import com.simple.banking.repository.CustomerRepository;
import com.simple.banking.service.abstracts.BankAccountService;
import com.simple.banking.util.BankAccountGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountImpl implements BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;
    @Autowired
    CustomerRepository customerRepository;

    ModelMapper modelMapper = new ModelMapper();



    @Override
    public BankAccountResponseDTO addBankAccount(BankAccountRequestDTO bankAccountRequestDTO) {
        BankAccount bankAccount = new BankAccount();

        String accountNumber = BankAccountGenerator.randomHesapNumarasi();
        bankAccount.setAccountNumber(accountNumber);
        bankAccount.setBalance(bankAccountRequestDTO.getBalance());

        Customer customer = customerRepository.findByIdentityNumber(bankAccountRequestDTO.getIdentityNumber());

        bankAccount.setCustomer(customer);
        BankAccount response= bankAccountRepository.save(bankAccount);
        BankAccountResponseDTO bankAccountResponseDTO= modelMapper.map(response, BankAccountResponseDTO.class);
        return bankAccountResponseDTO;
    }

    @Override
    public List<BankAccount> getAllBankAccount() {

        return bankAccountRepository.findAll();
    }
}
