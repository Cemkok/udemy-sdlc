package com.simple.banking.service.concretes;

import com.simple.banking.dto.request.BillResponseRecord;
import com.simple.banking.enums.BillType;
import com.simple.banking.model.Bill;
import com.simple.banking.service.abstracts.BillService;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
public class BillServiceImpl implements BillService {


    private final WebClient webClient;

    public BillServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("http://localhost:3002").build();
    }

    public Flux<BillResponseRecord> getBills() {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/get-bills")
                        .build())
                .retrieve()
                .bodyToFlux(BillResponseRecord.class);
    }

    @Override
    public List<Bill> getAllBills() {
        return null;
    }

    @Override
    public List<Bill> getBillsByType(BillType type) {
        return null;
    }

    @Override
    public boolean payBill(String billNumber) {
        return false;
    }

}
