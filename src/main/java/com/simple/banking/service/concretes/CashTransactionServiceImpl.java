package com.simple.banking.service.concretes;

import com.simple.banking.dto.request.CashTransactionRequestDTO;
import com.simple.banking.dto.response.CashTransactionResponseDTO;
import com.simple.banking.enums.CashTransactionType;
import com.simple.banking.model.BankAccount;
import com.simple.banking.model.CashTransaction;
import com.simple.banking.repository.BankAccountRepository;
import com.simple.banking.repository.CashTransactionRepository;
import com.simple.banking.service.abstracts.CashTransactionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.print.attribute.standard.Destination;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CashTransactionServiceImpl implements CashTransactionService {
    @Autowired
    private CashTransactionRepository cashTransactionRepository;
    @Autowired
    private BankAccountRepository bankAccountRepository;
    ModelMapper modelMapper = new ModelMapper();
    @Override
    public List<CashTransactionResponseDTO> findAll() {
        List<CashTransactionResponseDTO> destinationList = cashTransactionRepository.findAll().stream()
                .map(CashTransaction -> modelMapper.map(CashTransaction, CashTransactionResponseDTO.class))
                .toList();

        return destinationList;
    }

    public Optional<CashTransaction> findById(Long id) {
        return cashTransactionRepository.findById(id);
    }

    @Override
    @Transactional
    public CashTransactionResponseDTO deposit(CashTransactionRequestDTO cashTransactionRequestDTO) {
        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(cashTransactionRequestDTO.getBankAccountNumber())
                .orElseThrow(() -> new IllegalArgumentException("Invalid bank account ID"));
        CashTransaction cashTransaction = new CashTransaction();
        cashTransaction.setAmount(cashTransactionRequestDTO.getAmount());
        cashTransaction.setType(CashTransactionType.DEPOSIT);
        cashTransaction.setBankAccount(bankAccount);

        bankAccount.setBalance(bankAccount.getBalance() + cashTransaction.getAmount());
//        bankAccountRepository.save(bankAccount);

        return modelMapper.map(cashTransactionRepository.save(cashTransaction), CashTransactionResponseDTO.class);
    }

    @Override
    @Transactional
    public CashTransactionResponseDTO withdraw(CashTransactionRequestDTO cashTransactionRequestDTO) {
        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(cashTransactionRequestDTO.getBankAccountNumber())
                .orElseThrow(() -> new IllegalArgumentException("Invalid bank account ID"));

        if (bankAccount.getBalance() < cashTransactionRequestDTO.getAmount()) {
            throw new IllegalArgumentException("Insufficient balance");
        }

        CashTransaction cashTransaction = new CashTransaction();
        cashTransaction.setAmount(cashTransactionRequestDTO.getAmount());
        cashTransaction.setType(CashTransactionType.WITHDRAW);
        bankAccount.setBalance(bankAccount.getBalance() - cashTransaction.getAmount());
        cashTransaction.setBankAccount(bankAccount);

        return modelMapper.map(cashTransactionRepository.save(cashTransaction), CashTransactionResponseDTO.class);
    }

    @Transactional
    public void deleteById(Long id) {
        cashTransactionRepository.deleteById(id);
    }
}
