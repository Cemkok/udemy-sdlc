package com.simple.banking.service.concretes;


import com.simple.banking.dto.request.CreditApplicationRequest;
import com.simple.banking.dto.request.CreditScoreRequest;
import com.simple.banking.dto.response.CreditApplicationResponse;
import com.simple.banking.model.BankAccount;
import com.simple.banking.model.Credit;
import com.simple.banking.model.CreditApplication;
import com.simple.banking.model.Customer;
import com.simple.banking.repository.BankAccountRepository;
import com.simple.banking.repository.CreditApplicationRepository;
import com.simple.banking.repository.CreditRepository;
import com.simple.banking.repository.CustomerRepository;
import com.simple.banking.service.abstracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CreditApplicationService {

    @Autowired
    private CreditApplicationRepository creditApplicationRepository;
    @Autowired
    private CreditRepository creditRepository;
//    @Autowired
//    private CustomerRepository customerRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private CreditScoreService creditScoreService; // Kredi puanı sorgulama servisi

    public CreditApplicationResponse applyForCredit(CreditApplicationRequest request) {
        Credit credit = creditRepository.findById(request.getCreditId())
                .orElseThrow(() -> new RuntimeException("Credit not found"));

        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(request.getAccountNumber())
                .orElseThrow(() -> new RuntimeException("Bank Account not found"));

        CreditScoreRequest creditScoreRequest = new CreditScoreRequest();
        creditScoreRequest.setIdentityNumber(bankAccount.getCustomer().getIdentityNumber());

        // Kredi puanı sorgulaması yapılır
        int creditScore = creditScoreService.getCustomerCreditScore(creditScoreRequest);
        boolean isCreditApproved = creditScore>=credit.getMinimumCreditScore();

        CreditApplication application = new CreditApplication();
        application.setCredit(credit);
        application.setBankAccount(bankAccount);
        application.setAmount(request.getAmount());
        application.setStatus(isCreditApproved ? "Approved" : "Rejected");
        application.setApplicationDate(new Date());

        // Onaylanan veya reddedilen krediler için kayıt yapılır
        CreditApplication savedApplication = creditApplicationRepository.save(application);

        // Eğer kredi onaylanırsa işlem ücreti hesabından çekilir
        if (isCreditApproved) {
            // Müşterinin hesabına işlem ücreti düşülür (örneğin, banka hesabından)
            // Ödeme işlemleri burada yapılır, uygulamanıza göre bu kısım değişebilir
            // Örneğin: customer.deductProcessingFee(processingFee);

            savedApplication.setProcessingFee(calculateProcessingFee(request.getAmount(),credit.getProcessingFeeRate()));
            savedApplication.setTotalInterest(calculateTotalInterest(request.getAmount(), credit.getRate(), request.getTerm()));
            savedApplication.setTotalRepayment(calculateTotalRepayment(request.getAmount(), savedApplication.getTotalInterest()));
            savedApplication.setLoanDisbursementAmount(request.getAmount()-savedApplication.getProcessingFee());

            creditApplicationRepository.save(savedApplication); // Güncellenmiş bilgileri kaydet



        }

        CreditApplicationResponse response = new CreditApplicationResponse();
        response.setApplicationId(savedApplication.getId());
        response.setStatus(savedApplication.getStatus());
        response.setMessage("Application submitted successfully");
        return response;
    }

    public CreditApplicationResponse getCreditApplicationById(Long id) {
        CreditApplication application = creditApplicationRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Application not found"));

        CreditApplicationResponse response = new CreditApplicationResponse();
        response.setApplicationId(application.getId());
        response.setStatus(application.getStatus());
        response.setMessage("Application retrieved successfully");
        return response;
    }

    // Diğer kredi başvuru işlemleri

    // İşlem ücreti hesaplama metodu
    public double calculateProcessingFee(double amount, double processingFeeRate) {
        // İşlem ücreti hesaplama mantığı buraya gelecek
        return (amount * processingFeeRate ) / 100.0; // Örnek bir sabit ücret
    }

    // Toplam basit faiz hesaplama metodu
    public double calculateTotalInterest(double amount, double rate, int term) {
        // Toplam faiz hesaplama mantığı buraya gelecek
        return (amount * rate * term) / 100.0; // Örnek bir basit faiz hesaplama
    }

    // Toplam ödenecek tutarı hesaplama metodu
    public double calculateTotalRepayment(double amount, double totalInterest) {
        // Toplam ödenecek tutar hesaplama mantığı buraya gelecek
        return amount + totalInterest; // Kredi miktarı + toplam faiz
    }
}


