package com.simple.banking.service.concretes;


import com.simple.banking.dto.request.CreditScoreRequest;
import com.simple.banking.dto.response.CreditScoreResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
public class CreditScoreService {

//    @Value("${credit.score.mock.url}") // application.properties veya application.yml dosyasından alınacak URL
    private String mockServiceUrl="http://localhost:3001/approve";
    @Autowired
    private  RestTemplate restTemplate;


    public int getCustomerCreditScore(CreditScoreRequest requestDTO) {
        // HTTP headers oluşturma
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // HTTP request body oluşturma
        HttpEntity<CreditScoreRequest> requestEntity = new HttpEntity<>(requestDTO, headers);

        // POST isteği yapma
        ResponseEntity<CreditScoreResponse> responseEntity = restTemplate.exchange(
                mockServiceUrl,
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<CreditScoreResponse>() {}
        );

        return Objects.requireNonNull(responseEntity.getBody()).getCreditScore();
    }
}
