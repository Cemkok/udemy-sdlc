package com.simple.banking.service.concretes;

import com.simple.banking.dto.request.CreditRequestDTO;
import com.simple.banking.model.Credit;
import com.simple.banking.repository.CreditRepository;
import com.simple.banking.service.abstracts.CreditService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class CreditServiceImpl implements CreditService {

    @Autowired
    CreditRepository creditRepository;

    ModelMapper modelMapper = new ModelMapper();

    @Override
    public Credit addCredit(CreditRequestDTO creditRequestDTO) {
        Credit credit = modelMapper.map(creditRequestDTO, Credit.class);
        return creditRepository.save(credit);
    }

    @Override
    public List<Credit> getAllCredit() {
        return creditRepository.findAll();
    }

    @Override
    public Credit findCreditById(Long id) {
        return creditRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Credit not found with id: " + id));
    }

    @Override
    public Credit updateCredit(Credit credit) {
        return creditRepository.save(credit);
    }

    @Override
    public void deleteCredit(Long id) {
        Credit credit = findCreditById(id); // Check if credit exists
        creditRepository.delete(credit);
    }
}
