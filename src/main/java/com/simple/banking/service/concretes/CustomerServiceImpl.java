package com.simple.banking.service.concretes;

import com.simple.banking.dto.request.CustomerRequestDTO;
import com.simple.banking.dto.response.CustomerResponseDTO;
import com.simple.banking.model.Customer;
import com.simple.banking.repository.CustomerRepository;
import com.simple.banking.service.abstracts.CustomerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;
    ModelMapper modelMapper = new ModelMapper();

    @Override
    public CustomerResponseDTO addCustomer(CustomerRequestDTO customerRequestDTO) {

        Customer customer = new Customer();
        customer.setName(customerRequestDTO.getName());
        customer.setSurname(customerRequestDTO.getSurname());
        customer.setIdentityNumber(customerRequestDTO.getIdentityNumber());
        customer.setDateOfBirth(customerRequestDTO.getDateOfBirth());
        return  modelMapper.map(customerRepository.save(customer), CustomerResponseDTO.class);
    }

    @Override
    public List<CustomerResponseDTO> getAll() {
        List<Customer> customers = customerRepository.findAll();
        // Customer nesnelerini CustomerResponseDTO nesnelerine dönüştürün
        List<CustomerResponseDTO> customerResponseDTOs = customers.stream()
                .map(customer -> modelMapper.map(customer, CustomerResponseDTO.class))
                .toList();
        return customerResponseDTOs;
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        return customerRepository.save(customer);
    }
}
