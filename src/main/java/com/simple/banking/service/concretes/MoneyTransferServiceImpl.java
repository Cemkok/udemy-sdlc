package com.simple.banking.service.concretes;


import com.simple.banking.dto.request.ApprovalRequest;
import com.simple.banking.dto.response.ApprovalResponse;
import com.simple.banking.model.BankAccount;
import com.simple.banking.model.EFT;
import com.simple.banking.model.Havale;
import com.simple.banking.repository.BankAccountRepository;
import com.simple.banking.repository.EFTRepository;
import com.simple.banking.repository.HavaleRepository;
import com.simple.banking.service.abstracts.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

@Service
public class MoneyTransferServiceImpl implements MoneyTransferService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private HavaleRepository havaleRepository;

    @Autowired
    private EFTRepository eftRepository;

    @Autowired
    private RestTemplate restTemplate; // RestTemplate ile başka bankanın onay servisine istek gönderebiliriz

    @Transactional
    @Override
    public boolean makeHavale(String sourceAccountNumber, String targetAccountNumber, Double amount) {
        try {
            BankAccount sourceAccount = bankAccountRepository.findByAccountNumber(sourceAccountNumber)
                    .orElseThrow(() -> new RuntimeException("Source account not found"));

            BankAccount targetAccount = bankAccountRepository.findByAccountNumber(targetAccountNumber)
                    .orElseThrow(() -> new RuntimeException("Target account not found"));

            if (sourceAccount.getBalance() < amount) {
                throw new RuntimeException("Insufficient funds");
            }

            // Update balances for havale
            sourceAccount.setBalance(sourceAccount.getBalance() - amount);
            targetAccount.setBalance(targetAccount.getBalance() + amount);

            // Create the transfer record for havale
            Havale havale = new Havale();
            havale.setSourceAccount(sourceAccount);
            havale.setTargetAccount(targetAccount);
            havale.setAmount(amount);
            havale.setTransactionDate(LocalDateTime.now());

            // Save the transfer and update accounts
            havaleRepository.save(havale);
//            bankAccountRepository.save(sourceAccount);
//            bankAccountRepository.save(targetAccount);
            return true;
        } catch (Exception e) {
            // Log the exception (optional)
            return false;
        }
    }

    @Transactional
    @Override
    public boolean makeEFT(String sourceAccountNumber, String targetAccountNumber, Double amount) {
        try {
            BankAccount sourceAccount = bankAccountRepository.findByAccountNumber(sourceAccountNumber)
                    .orElseThrow(() -> new RuntimeException("Source account not found"));

            if (sourceAccount.getBalance() < amount) {
                throw new RuntimeException("Insufficient funds");
            }

            // Hedef bankadan onay al
            boolean isApproved = getApprovalFromTargetBank(targetAccountNumber, amount);
            if (!isApproved) {
                throw new RuntimeException("Transfer not approved by the target bank");
            }

            // Update source account balance for EFT
            sourceAccount.setBalance(sourceAccount.getBalance() - amount);

            // Create the transfer record for EFT
            EFT eft = new EFT();
            eft.setSourceAccount(sourceAccount);
            eft.setTargetAccountNumber(targetAccountNumber); // Hedef hesap numarasını saklıyoruz
            eft.setAmount(amount);
            eft.setTransactionDate(LocalDateTime.now());

            // Save the transfer and update the source account
            eftRepository.save(eft);
//            bankAccountRepository.save(sourceAccount);
            return true;
        } catch (Exception e) {
            // Log the exception (optional)
            return false;
        }
    }

    private boolean getApprovalFromTargetBank(String targetAccountNumber, Double amount) {
        // Örnek bir REST API çağrısı (detayları hedef bankanın API'sine göre değiştirilmelidir)
//        String targetBankApiUrl = "https://api.targetbank.com/approve";
        String targetBankApiUrl = "http://localhost:3001/approve";
        ApprovalRequest request = new ApprovalRequest(targetAccountNumber, amount);
        ResponseEntity<ApprovalResponse> responseEntity = restTemplate.postForEntity(targetBankApiUrl, request, ApprovalResponse.class);
        ApprovalResponse response = responseEntity.getBody();


//        return response != null && response.isApproved();
        return response.isApproved();

    }
}
