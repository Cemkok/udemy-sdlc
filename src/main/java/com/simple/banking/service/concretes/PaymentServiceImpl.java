package com.simple.banking.service.concretes;

import com.simple.banking.model.BankAccount;
import com.simple.banking.model.Bill;
import com.simple.banking.repository.BankAccountRepository;
import com.simple.banking.repository.PaymentRepository;
import com.simple.banking.service.abstracts.BillService;
import com.simple.banking.service.abstracts.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final BankAccountRepository bankAccountRepository;
    private final BillService billService;
    private final PaymentRepository paymentRepository;


    public boolean payBill(String accountNumber, String billNumber) {
        // Hesap ve fatura bilgilerini al
        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(accountNumber).get();
        Bill bill = billService.getBillDetails(billNumber);

        // Bakiyenin yeterli olup olmadığını kontrol et
        if (bankAccount.getBalance() >= bill.getAmount()) {
            // Yeterli bakiye varsa ödemeyi gerçekleştir ve bakiyeden düş
            bankAccount.setBalance(bankAccount.getBalance() - bill.getAmount());
            accountService.updateAccountBalance(account);

            // Ödeme kaydını veritabanına ekle
            Payment payment = new Payment(accountNumber, billNumber, bill.getAmount());
            paymentRepository.save(payment);

            // Fatura merkezi servisine ödeme bildirimini gönder
            notifyBillService(billNumber, bill.getAmount());

            return true; // Ödeme başarılı
        } else {
            // Yeterli bakiye yoksa ödeme başarısız
            return false;
        }
    }

    private void notifyBillService(String billNumber, double amount) {
        String url = billServiceUrl + "/bills/" + billNumber + "/pay";
        Map<String, Object> request = new HashMap<>();
        request.put("amount", amount);

        restTemplate.postForEntity(url, request, Void.class);
    }

}
