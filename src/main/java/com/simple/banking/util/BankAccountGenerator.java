package com.simple.banking.util;

import java.util.Random;

public class BankAccountGenerator {
    public static String randomHesapNumarasi() {
        Random random = new Random();
        StringBuilder hesapNumarasi = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            int rakam = random.nextInt(10); // 0 ile 9 arasında rastgele bir rakam üret
            hesapNumarasi.append(rakam);
        }
        return hesapNumarasi.toString();
    }
}
